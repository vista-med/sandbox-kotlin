package com.ivolodin.vista

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VistaApplication

fun main(args: Array<String>) {
    runApplication<VistaApplication>(*args)
}
