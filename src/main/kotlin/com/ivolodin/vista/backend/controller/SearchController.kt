package com.ivolodin.vista.backend.controller

import com.ivolodin.vista.backend.model.request.SearchRequest
import com.ivolodin.vista.backend.model.response.SearchResponse
import com.ivolodin.vista.backend.service.SearchService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/search")
class SearchController(
    var searchService: SearchService
){
    @PostMapping
    fun searchForPatients(@RequestBody searchRequest: SearchRequest): List<SearchResponse> =
        searchService.searchForPatients(searchRequest)
}