package com.ivolodin.vista.backend.controller

import com.ivolodin.vista.backend.service.DoctorService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.NotEmpty

@RestController
@RequestMapping("/doctor")
class DoctorController(
    var doctorService: DoctorService
) {

    @GetMapping
    fun getAllDoctorNamesStartingFrom(@RequestParam name: String) : List<String>{
        return doctorService.findByNameStartingFrom(name)
    }
}