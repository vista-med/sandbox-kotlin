package com.ivolodin.vista.backend.repository

import com.ivolodin.vista.backend.model.entity.EventEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDate
import java.util.*

interface EventRepository : JpaRepository<EventEntity, Int> {

    @Query(
        //language=HQL
        value = "SELECT e from EventEntity e where " +
                "e.createPerson.id = :setPersonId and " +
                "e.eventType.id = :eventTypeId and  " +
                "cast(e.setDate as date) = :date"
    )
    fun findAllEventsBySetPersonIdAndEventTypeIdAndExecDate(
        setPersonId: Int,
        eventTypeId: Int,
        date: Date
    ): List<EventEntity>
}