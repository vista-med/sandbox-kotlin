package com.ivolodin.vista.backend.repository

import com.ivolodin.vista.backend.model.entity.PersonEntity
import org.springframework.data.jpa.repository.JpaRepository

interface DoctorRepository : JpaRepository<PersonEntity, Long> {
    fun findFirstByLastNameAndFirstNameAndPatrName(lastName: String, firstName: String, patrName: String): PersonEntity?

    fun findAllByLastNameContainingAndFirstNameContainingAndPatrNameContaining(lastName: String, firstName: String, patrName: String): List<PersonEntity>
}