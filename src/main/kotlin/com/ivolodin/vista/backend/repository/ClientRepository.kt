package com.ivolodin.vista.backend.repository

import com.ivolodin.vista.backend.model.entity.ClientEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ClientRepository: JpaRepository<ClientEntity, Int>{
}