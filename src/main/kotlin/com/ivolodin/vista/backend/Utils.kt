package com.ivolodin.vista.backend


fun List<String>.getOrEmptyString(i: Int): String {
    return if (this.size <= i)
        ""
    else
        this[i]
}