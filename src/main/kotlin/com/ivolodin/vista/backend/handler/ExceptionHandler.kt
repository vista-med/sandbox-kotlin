package com.ivolodin.vista.backend.handler

import com.ivolodin.vista.backend.model.ApiException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [ApiException::class])
    fun apiExceptionHandler(ex: ApiException, request: WebRequest): ResponseEntity<Exception> {
        return ResponseEntity(Exception(ex.message), HttpStatus.BAD_REQUEST)
    }
}