package com.ivolodin.vista.backend.service

import com.github.mvysny.vokdataloader.splitToWords
import com.ivolodin.vista.backend.getOrEmptyString
import com.ivolodin.vista.backend.model.ApiException
import com.ivolodin.vista.backend.model.entity.ClientEntity
import com.ivolodin.vista.backend.model.entity.EventEntity
import com.ivolodin.vista.backend.model.entity.PersonEntity
import com.ivolodin.vista.backend.model.request.SearchRequest
import com.ivolodin.vista.backend.model.response.SearchResponse
import com.ivolodin.vista.backend.repository.ClientRepository
import com.ivolodin.vista.backend.repository.DoctorRepository
import com.ivolodin.vista.backend.repository.EventRepository
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

@Component
@RequiredArgsConstructor
class SearchService {

    @Autowired
    private lateinit var eventRepository: EventRepository

    @Autowired
    private lateinit var clientRepository: ClientRepository

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    fun searchForPatients(request: SearchRequest): List<SearchResponse> {
        val doctor = doctorRepository.findFirstByLastNameAndFirstNameAndPatrName(
            request.doctorFullName.splitToWords().getOrEmptyString(0),
            request.doctorFullName.splitToWords().getOrEmptyString(1),
            request.doctorFullName.splitToWords().getOrEmptyString(2),
        )
        if (doctor?.id == null)
            throw ApiException("Doctor hasn't been found")

        val eventList = eventRepository.findAllEventsBySetPersonIdAndEventTypeIdAndExecDate(
            doctor.id!!,
            29,
            request.dateToShow.toDate()
        )
        val clientIdList = getEventIds(eventList)
        val clientList = clientRepository.findAllById(clientIdList)

        val eventToClientSet =
            clientList.associate { it.id to eventList.first { eventEntity -> eventEntity.client?.id == it.id } }

        return clientList.mapNotNull { buildSearchResponse(it, doctor, eventToClientSet[it.id]!!) }
    }

    private fun buildSearchResponse(client: ClientEntity, doctor: PersonEntity, event: EventEntity): SearchResponse =
        SearchResponse(
            client.firstName,
            client.lastName,
            client.patrName,
            doctor.firstName,
            doctor.lastName,
            doctor.patrName,
            event.eventType?.name.orEmpty(),
            event.setDate
        )

    private fun getEventIds(events: List<EventEntity>): List<Int> =
        events.mapNotNull { it.client }
            .mapNotNull { it.id }

    fun LocalDate.toDate() = Date.from(
        this.atStartOfDay()
            .atZone(ZoneId.systemDefault())
            .toInstant()
    )!!

}