package com.ivolodin.vista.backend.service

import com.github.mvysny.vokdataloader.splitToWords
import com.ivolodin.vista.backend.getOrEmptyString
import com.ivolodin.vista.backend.repository.DoctorRepository
import org.springframework.stereotype.Component

@Component
class DoctorService(
    var doctorRepository: DoctorRepository
) {

    fun findByNameStartingFrom(name: String): List<String> {
        val lastName = name.splitToWords().getOrEmptyString(0)
        val firstName = name.splitToWords().getOrEmptyString(1)
        val patrName = name.splitToWords().getOrEmptyString(2)

        return doctorRepository.findAllByLastNameContainingAndFirstNameContainingAndPatrNameContaining(lastName, firstName, patrName)
            .map { it.getFullName() }

    }
}
