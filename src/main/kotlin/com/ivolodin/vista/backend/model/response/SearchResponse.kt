package com.ivolodin.vista.backend.model.response

import lombok.Builder
import lombok.With
import java.time.LocalDateTime

@With
@Builder
data class SearchResponse(
    var clientFirstName: String = "",
    var clientLastName: String = "",
    var clientPatronymicName: String = "",
    var doctorFirstName: String = "",
    var doctorLastName: String = "",
    var doctorPatronymicName: String = "",
    var eventTypeName: String = "",
    var eventDate: LocalDateTime? = null
) {
    fun getDoctorFullName() = "$doctorLastName $doctorFirstName $doctorPatronymicName"

    fun getClientFullName() = "$clientLastName $clientFirstName $clientPatronymicName"
}
