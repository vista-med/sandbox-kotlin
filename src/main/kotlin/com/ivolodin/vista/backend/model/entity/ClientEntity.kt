package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.*

@Table(name = "Client")
@Entity
open class ClientEntity(
    @Column(name = "createDatetime", nullable = false)
    open val createDatetime: Instant,

    @Column(name = "modifyDatetime", nullable = false)
    open val modifyDatetime: Instant? = null,
    @Column(name = "lastName", nullable = false, length = 30)
    open val lastName: String,

    @Column(name = "firstName", nullable = false, length = 30)
    open val firstName: String,

    @Column(name = "patrName", nullable = false, length = 64)
    open val patrName: String,
    ) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open val id: Int? = null


    @Column(name = "createPerson_id")
    open var createpersonId: Int? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @ManyToOne
    @JoinColumn(name = "attendingPerson_id")
    open var attendingPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false


    @Column(name = "birthDate", nullable = false)
    open var birthDate: LocalDate? = null

    @Column(name = "birthTime", nullable = false)
    open var birthTime: LocalTime? = null

    @Column(name = "sex", nullable = false)
    open var sex: Boolean? = false

    @Column(name = "SNILS", nullable = false, length = 11)
    open var snils: String? = null

    @Column(name = "bloodDate")
    open var bloodDate: LocalDate? = null

    @Column(name = "bloodNotes", nullable = false)
    open var bloodNotes: String? = null

    @Column(name = "growth", nullable = false, length = 16)
    open var growth: String? = null

    @Column(name = "weight", nullable = false, length = 16)
    open var weight: String? = null

    @Column(name = "embryonalPeriodWeek", nullable = false, length = 16)
    open var embryonalPeriodWeek: String? = null

    @Column(name = "birthPlace", nullable = false, length = 120)
    open var birthPlace: String? = null

    @Column(name = "chronicalMKB", nullable = false, length = 8)
    open var chronicalMKB: String? = null

    @Column(name = "diagNames", nullable = false, length = 64)
    open var diagNames: String? = null

    @Column(name = "chartBeginDate")
    open var chartBeginDate: LocalDate? = null

    @Column(name = "notes", nullable = false)
    open var notes: String? = null

    @Column(name = "IIN", length = 15)
    open var iin: String? = null

    @Column(name = "isConfirmSendingData")
    open var isConfirmSendingData: Boolean? = null

    @Column(name = "isUnconscious")
    open var isUnconscious: Boolean? = null

    @Column(name = "hasImplants")
    open var hasImplants: Boolean? = null

    @Column(name = "hasProsthesis")
    open var hasProsthesis: Boolean? = null

    @Column(name = "lastExportDatetime")
    open var lastExportDatetime: Instant? = null

    @Column(name = "filial")
    open var filial: Int? = null

    @Column(name = "dataTransferConfirmationDate")
    open var dataTransferConfirmationDate: LocalDate? = null

    @Column(name = "UUID", length = 36)
    open var uuid: String? = null

    @Column(name = "race")
    open var race: Int? = null

    @Column(name = "IEMK_guid", length = 64)
    open var iemkGuid: String? = null

    @Column(name = "SNILSMissing_id")
    open var snilsmissingId: Int? = null

    @Column(name = "dontSendVIMIS", nullable = false)
    open var dontSendVIMIS: Boolean? = false
}