package com.ivolodin.vista.backend.model.request

import java.time.LocalDate

data class SearchRequest(
    val doctorFullName: String,
    val dateToShow: LocalDate
)
