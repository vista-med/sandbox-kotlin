package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import javax.persistence.*

@Table(name = "OrgStructure")
@Entity
open class OrgStructureEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @ManyToOne(optional = false)
    @JoinColumn(name = "organisation_id", nullable = false)
    open var organisation: OrganisationEntity? = null

    @Column(name = "code", nullable = false, length = 256)
    open var code: String? = null

    @Column(name = "name", nullable = false, length = 256)
    open var name: String? = null

    @ManyToOne
    @JoinColumn(name = "parent_id")
    open var parent: OrgStructureEntity? = null

    @Column(name = "type", nullable = false)
    open var type: Int? = null
    
    @Column(name = "chief_id")
    open var chiefId: Int? = null

    @ManyToOne
    @JoinColumn(name = "headNurse_id")
    open var headNurse: PersonEntity? = null

    @Column(name = "isArea", nullable = false)
    open var isArea: Boolean? = false

    @Column(name = "hasHospitalBeds", nullable = false)
    open var hasHospitalBeds: Boolean? = false

    @Column(name = "hasStocks", nullable = false)
    open var hasStocks: Boolean? = false

    @Column(name = "hasDayStationary", nullable = false)
    open var hasDayStationary: Boolean? = false

    @Column(name = "infisCode", nullable = false, length = 16)
    open var infisCode: String? = null

    @Column(name = "infisInternalCode", nullable = false, length = 30)
    open var infisInternalCode: String? = null

    @Column(name = "infisDepTypeCode", nullable = false, length = 30)
    open var infisDepTypeCode: String? = null

    @Column(name = "availableForExternal", nullable = false)
    open var availableForExternal: Int? = null

    @Column(name = "Address", nullable = false, length = 250)
    open var address: String? = null

    @Column(name = "infisTariffCode", nullable = false, length = 16)
    open var infisTariffCode: String? = null

    @Column(name = "inheritEventTypes", nullable = false)
    open var inheritEventTypes: Boolean? = false

    @Column(name = "inheritActionTypes", nullable = false)
    open var inheritActionTypes: Boolean? = false

    @Column(name = "inheritGaps", nullable = false)
    open var inheritGaps: Boolean? = false

    @Column(name = "bookkeeperCode", nullable = false, length = 16)
    open var bookkeeperCode: String? = null

    @Column(name = "dayLimit")
    open var dayLimit: Boolean? = null

    @Column(name = "storageCode", nullable = false, length = 64)
    open var storageCode: String? = null

    @Column(name = "miacHead_id")
    open var miacheadId: Int? = null

    @Column(name = "prefix", length = 50)
    open var prefix: String? = null

    @Column(name = "salaryPercentage")
    open var salaryPercentage: Int? = null

    @Column(name = "attachCode")
    open var attachCode: Int? = null

    @Column(name = "isVisibleInDR")
    open var isVisibleInDR: Boolean? = null

    @Column(name = "tfomsCode", length = 16)
    open var tfomsCode: String? = null

    @Lob
    @Column(name = "syncGUID")
    open var syncGUID: String? = null

    @Column(name = "quota")
    open var quota: Boolean? = null

    @Column(name = "miacCode", length = 11)
    open var miacCode: String? = null

    @Column(name = "netrica_Code", length = 64)
    open var netricaCode: String? = null

    @Column(name = "filial")
    open var filial: Int? = null

    @Column(name = "idLPU_egisz", length = 11)
    open var idlpuEgisz: String? = null

    @Column(name = "netrica_Code_UO", nullable = false, length = 64)
    open var netricaCodeUo: String? = null

    @Column(name = "idxLocationCard")
    open var idxLocationCard: Int? = null

    @Column(name = "isVisibleInLocationCard")
    open var isVisibleInLocationCard: Boolean? = null

    @Column(name = "FRMOcode", nullable = false, length = 512)
    open var fRMOcode: String? = null

    @Column(name = "netrica_Code_IEMK", length = 64)
    open var netricaCodeIemk: String? = null

    @Column(name = "level")
    open var level: Int? = null

    @Column(name = "phoneNumber", length = 64)
    open var phoneNumber: String? = null
}