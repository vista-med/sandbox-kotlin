package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import javax.persistence.*

@Table(name = "Visit")
@Entity
open class VisitEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @ManyToOne(optional = false)
    @JoinColumn(name = "event_id", nullable = false)
    open var event: EventEntity? = null

    @Column(name = "date", nullable = false)
    open var date: Instant? = null

    @ManyToOne(optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    open var person: PersonEntity? = null

    @Column(name = "isPrimary", nullable = false)
    open var isPrimary: Boolean? = false

    @ManyToOne(optional = false)
    @JoinColumn(name = "finance_id", nullable = false)
    open var finance: RbFinanceEntity? = null

    @Column(name = "payStatus", nullable = false)
    open var payStatus: Int? = null

    @ManyToOne
    @JoinColumn(name = "assistant_id")
    open var assistant: PersonEntity? = null

    @Column(name = "MKB", length = 8)
    open var mkb: String? = null

    @Column(name = "komtek_id")
    open var komtekId: Int? = null
}