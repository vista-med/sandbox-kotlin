package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import javax.persistence.*

@Table(name = "Action")
@Entity
open class ActionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "parent_id")
    open var parentId: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @ManyToOne(optional = false)
    @JoinColumn(name = "actionType_id", nullable = false)
    open var actionType: ActionTypeEntity? = null

    @Column(name = "specifiedName", nullable = false)
    open var specifiedName: String? = null

    @ManyToOne
    @JoinColumn(name = "event_id")
    open var event: EventEntity? = null

    @Column(name = "idx", nullable = false)
    open var idx: Int? = null

    @Column(name = "directionDate")
    open var directionDate: Instant? = null

    @Column(name = "status", nullable = false)
    open var status: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "setPerson_id")
    open var setPerson: PersonEntity? = null

    @Column(name = "isUrgent", nullable = false)
    open var isUrgent: Int? = null

    @Column(name = "begDate")
    open var begDate: Instant? = null

    @Column(name = "plannedEndDate", nullable = false)
    open var plannedEndDate: Instant? = null

    @Column(name = "endDate")
    open var endDate: Instant? = null

    @Lob
    @Column(name = "note", nullable = false)
    open var note: String? = null

    @ManyToOne
    @JoinColumn(name = "person_id")
    open var person: PersonEntity? = null

    @Column(name = "office", nullable = false, length = 16)
    open var office: String? = null

    @Column(name = "amount", nullable = false)
    open var amount: Double? = null

    @Column(name = "uet")
    open var uet: Double? = null

    @Column(name = "payStatus", nullable = false)
    open var payStatus: Int? = null

    @Column(name = "account", nullable = false)
    open var account: Boolean? = false

    @Column(name = "MKB", nullable = false, length = 8)
    open var mkb: String? = null

    @Column(name = "morphologyMKB", nullable = false, length = 16)
    open var morphologyMKB: String? = null

    @ManyToOne
    @JoinColumn(name = "finance_id")
    open var finance: RbFinanceEntity? = null

    @ManyToOne
    @JoinColumn(name = "prescription_id")
    open var prescription: ActionEntity? = null

    @ManyToOne
    @JoinColumn(name = "org_id")
    open var org: OrganisationEntity? = null

    @Column(name = "coordDate")
    open var coordDate: Instant? = null

    @Column(name = "coordAgent", nullable = false, length = 128)
    open var coordAgent: String? = null

    @Column(name = "coordInspector", nullable = false, length = 128)
    open var coordInspector: String? = null

    @Column(name = "coordText", nullable = false)
    open var coordText: String? = null

    @ManyToOne
    @JoinColumn(name = "assistant_id")
    open var assistant: PersonEntity? = null

    @Column(name = "preliminaryResult", nullable = false)
    open var preliminaryResult: Boolean? = false

    @Column(name = "duration", nullable = false)
    open var duration: Boolean? = false

    @Column(name = "periodicity", nullable = false)
    open var periodicity: Boolean? = false

    @Column(name = "aliquoticity", nullable = false)
    open var aliquoticity: Boolean? = false

    @Column(name = "signature")
    open var signature: Int? = null

    @ManyToOne
    @JoinColumn(name = "assistant2_id")
    open var assistant2: PersonEntity? = null

    @ManyToOne
    @JoinColumn(name = "assistant3_id")
    open var assistant3: PersonEntity? = null

    @Column(name = "MES_id")
    open var mesId: Int? = null

    @Column(name = "counterValue", length = 30)
    open var counterValue: String? = null

    @Column(name = "customSum", nullable = false)
    open var customSum: Double? = null

    @Column(name = "isVerified")
    open var isVerified: Boolean? = null

    @Column(name = "notificationStatus", nullable = false)
    open var notificationStatus: Boolean? = false

    @Column(name = "operScope")
    open var operScope: Boolean? = null

    @Column(name = "EGISZ_ids", length = 100)
    open var egiszIds: String? = null

    @Column(name = "UUID", length = 36)
    open var uuid: String? = null

    @Column(name = "komtek_id")
    open var komtekId: Int? = null

    @Column(name = "komtekFile_id")
    open var komtekfileId: Int? = null

    @Column(name = "komtekFile_identifier", length = 20)
    open var komtekfileIdentifier: String? = null

    @Column(name = "vista_system")
    open var vistaSystem: Boolean? = null

    @Column(name = "signed")
    open var signed: Boolean? = null
}