package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import java.time.LocalDate
import javax.persistence.*

@Table(name = "Organisation")
@Entity
open class OrganisationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @Column(name = "createPerson_id")
    open var createpersonId: Int? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @Column(name = "modifyPerson_id")
    open var modifypersonId: Int? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @Column(name = "fullName", nullable = false, length = 636)
    open var fullName: String? = null

    @Column(name = "shortName", nullable = false, length = 636)
    open var shortName: String? = null

    @Column(name = "title", nullable = false, length = 636)
    open var title: String? = null

    @Column(name = "net_id")
    open var netId: Int? = null

    @Column(name = "infisCode", nullable = false, length = 12)
    open var infisCode: String? = null

    @Column(name = "obsoleteInfisCode", nullable = false, length = 60)
    open var obsoleteInfisCode: String? = null

    @Column(name = "OKVED", nullable = false, length = 64)
    open var okved: String? = null

    @Column(name = "INN", nullable = false, length = 15)
    open var inn: String? = null

    @Column(name = "KPP", nullable = false, length = 15)
    open var kpp: String? = null

    @Column(name = "OGRN", nullable = false, length = 15)
    open var ogrn: String? = null

    @Column(name = "OKATO", nullable = false, length = 15)
    open var okato: String? = null

    @Column(name = "OKPF_code", nullable = false, length = 4)
    open var okpfCode: String? = null

    @Column(name = "OKPF_id")
    open var okpfId: Int? = null

    @Column(name = "OKFS_code", nullable = false)
    open var okfsCode: Int? = null

    @Column(name = "OKFS_id")
    open var okfsId: Int? = null

    @Column(name = "OKPO", nullable = false, length = 15)
    open var okpo: String? = null

    @Column(name = "FSS", nullable = false, length = 10)
    open var fss: String? = null

    @Column(name = "region", nullable = false, length = 25)
    open var region: String? = null

    @Column(name = "Address", nullable = false, length = 250)
    open var address: String? = null

    @Column(name = "chief", nullable = false, length = 64)
    open var chief: String? = null

    @Column(name = "phone", nullable = false, length = 64)
    open var phone: String? = null

    @Column(name = "accountant", nullable = false, length = 64)
    open var accountant: String? = null

    @Column(name = "isInsurer", nullable = false)
    open var isInsurer: Boolean? = false

    @Column(name = "isCompulsoryInsurer", nullable = false)
    open var isCompulsoryInsurer: Boolean? = false

    @Column(name = "isVoluntaryInsurer", nullable = false)
    open var isVoluntaryInsurer: Boolean? = false

    @Column(name = "area", nullable = false, length = 13)
    open var area: String? = null

    @Column(name = "compulsoryServiceStop", nullable = false)
    open var compulsoryServiceStop: Boolean? = false

    @Column(name = "voluntaryServiceStop", nullable = false)
    open var voluntaryServiceStop: Boolean? = false

    @Column(name = "isHospital", nullable = false)
    open var isHospital: Boolean? = false

    @Column(name = "notes", nullable = false)
    open var notes: String? = null

    @Column(name = "head_id")
    open var headId: Int? = null

    @Column(name = "miacCode", nullable = false, length = 10)
    open var miacCode: String? = null

    @Column(name = "isMedical", nullable = false)
    open var isMedical: Boolean? = false

    @Column(name = "isArmyOrg", nullable = false)
    open var isArmyOrg: Boolean? = false

    @Column(name = "canOmitPolicyNumber", nullable = false)
    open var canOmitPolicyNumber: Boolean? = false

    @Column(name = "netrica_Code", length = 64)
    open var netricaCode: String? = null

    @Column(name = "DATN", nullable = false)
    open var datn: LocalDate? = null

    @Column(name = "DATO", nullable = false)
    open var dato: LocalDate? = null

    @Column(name = "reestrNumber")
    open var reestrNumber: Int? = null

    @Column(name = "EGISZ_code", nullable = false, length = 512)
    open var egiszCode: String? = null

    @Column(name = "isMSEBureau")
    open var isMSEBureau: Int? = null

    @Column(name = "mse_code", length = 64)
    open var mseCode: String? = null
}