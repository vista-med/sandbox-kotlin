package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import java.time.LocalTime
import javax.persistence.*

@Table(name = "EventType")
@Entity
open class EventTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @Column(name = "code", nullable = false, length = 8)
    open var code: String? = null

    @Column(name = "name", nullable = false, length = 200)
    open var name: String? = null

    @ManyToOne
    @JoinColumn(name = "finance_id")
    open var finance: RbFinanceEntity? = null

    @Column(name = "visitServiceModifier", nullable = false, length = 128)
    open var visitServiceModifier: String? = null

    @Column(name = "visitServiceFilter", nullable = false, length = 32)
    open var visitServiceFilter: String? = null

    @Column(name = "visitFinance", nullable = false)
    open var visitFinance: Boolean? = false

    @Column(name = "actionFinance", nullable = false)
    open var actionFinance: Boolean? = false

    @Column(name = "actionContract", nullable = false)
    open var actionContract: Boolean? = false

    @Column(name = "period", nullable = false)
    open var period: Boolean? = false

    @Column(name = "singleInPeriod", nullable = false)
    open var singleInPeriod: Boolean? = false

    @Column(name = "isLong", nullable = false)
    open var isLong: Boolean? = false

    @Column(name = "dateInput", nullable = false)
    open var dateInput: Boolean? = false

    @Column(name = "context", nullable = false, length = 64)
    open var context: String? = null

    @Column(name = "form", nullable = false, length = 64)
    open var form: String? = null

    @Column(name = "minDuration", nullable = false)
    open var minDuration: Int? = null

    @Column(name = "maxDuration", nullable = false)
    open var maxDuration: Int? = null

    @Column(name = "showStatusActionsInPlanner", nullable = false)
    open var showStatusActionsInPlanner: Boolean? = false

    @Column(name = "showDiagnosticActionsInPlanner", nullable = false)
    open var showDiagnosticActionsInPlanner: Boolean? = false

    @Column(name = "showCureActionsInPlanner", nullable = false)
    open var showCureActionsInPlanner: Boolean? = false

    @Column(name = "showMiscActionsInPlanner", nullable = false)
    open var showMiscActionsInPlanner: Boolean? = false

    @Column(name = "limitStatusActionsInput", nullable = false)
    open var limitStatusActionsInput: Boolean? = false

    @Column(name = "limitDiagnosticActionsInput", nullable = false)
    open var limitDiagnosticActionsInput: Boolean? = false

    @Column(name = "limitCureActionsInput", nullable = false)
    open var limitCureActionsInput: Boolean? = false

    @Column(name = "limitMiscActionsInput", nullable = false)
    open var limitMiscActionsInput: Boolean? = false

    @Column(name = "showTime", nullable = false)
    open var showTime: Boolean? = false

    @Column(name = "mesRequired", nullable = false)
    open var mesRequired: Int? = null

    @Column(name = "defaultMesSpecification_id")
    open var defaultmesspecificationId: Int? = null

    @Column(name = "mesCodeMask", length = 64)
    open var mesCodeMask: String? = null

    @Column(name = "mesNameMask", length = 64)
    open var mesNameMask: String? = null

    @Column(name = "isExternal", nullable = false)
    open var isExternal: Boolean? = false

    @Column(name = "generateExternalIdOnSave", nullable = false)
    open var generateExternalIdOnSave: Boolean? = false

    @Column(name = "externalIdAsAccountNumber", nullable = false)
    open var externalIdAsAccountNumber: Boolean? = false

    @Column(name = "counterType", nullable = false)
    open var counterType: Boolean? = false

    @Column(name = "hasAssistant", nullable = false)
    open var hasAssistant: Boolean? = false

    @Column(name = "hasCurator", nullable = false)
    open var hasCurator: Boolean? = false

    @Column(name = "hasVisitAssistant", nullable = false)
    open var hasVisitAssistant: Boolean? = false

    @Column(name = "canHavePayableActions", nullable = false)
    open var canHavePayableActions: Boolean? = false

    @Column(name = "isRequiredCoordination", nullable = false)
    open var isRequiredCoordination: Boolean? = false

    @Column(name = "isOrgStructurePriority", nullable = false)
    open var isOrgStructurePriority: Boolean? = false

    @Column(name = "isTakenTissue", nullable = false)
    open var isTakenTissue: Boolean? = false

    @Column(name = "isSetContractNumFromCounter")
    open var isSetContractNumFromCounter: Boolean? = null

    @Column(name = "sex", nullable = false)
    open var sex: Boolean? = false

    @Column(name = "age", nullable = false, length = 80)
    open var age: String? = null

    @Column(name = "isOnJobPayedFilter", nullable = false)
    open var isOnJobPayedFilter: Boolean? = false

    @Column(name = "permitAnyActionDate", nullable = false)
    open var permitAnyActionDate: Boolean? = false

    @Column(name = "prefix", length = 8)
    open var prefix: String? = null

    @Column(name = "exposeGrouped", nullable = false)
    open var exposeGrouped: Boolean? = false

    @Column(name = "showLittleStranger", nullable = false)
    open var showLittleStranger: Boolean? = false

    @Column(name = "uniqueExternalId", nullable = false)
    open var uniqueExternalId: Boolean? = false

    @Column(name = "uniqueExternalIdInThisYear")
    open var uniqueExternalIdInThisYear: Boolean? = null

    @Column(name = "defaultOrder", nullable = false)
    open var defaultOrder: Boolean? = false

    @Column(name = "inheritDiagnosis", nullable = false)
    open var inheritDiagnosis: Boolean? = false

    @Column(name = "diagnosisSetDateVisible", nullable = false)
    open var diagnosisSetDateVisible: Int? = null

    @Column(name = "isResetSetDate", nullable = false)
    open var isResetSetDate: Boolean? = false

    @Column(name = "isAvailInFastCreateMode", nullable = false)
    open var isAvailInFastCreateMode: Boolean? = false

    @Column(name = "defaultEndTime")
    open var defaultEndTime: LocalTime? = null

    @Column(name = "isCheck_KSG")
    open var ischeckKsg: Boolean? = null

    @Column(name = "weekdays", nullable = false)
    open var weekdays: Boolean? = false

    @Column(name = "exposeConfirmation", nullable = false)
    open var exposeConfirmation: Boolean? = false

    @Column(name = "showZNO")
    open var showZNO: Boolean? = null

    @Column(name = "goalFilter")
    open var goalFilter: Boolean? = null

    @Column(name = "needMesPerformPercent", nullable = false)
    open var needMesPerformPercent: Boolean? = false

    @Column(name = "setFilterStandard", nullable = false)
    open var setFilterStandard: Boolean? = false

    @Column(name = "purposeFilter")
    open var purposeFilter: Boolean? = null

    @Column(name = "inheritResult")
    open var inheritResult: Boolean? = null

    @Column(name = "inheritCheckupResult")
    open var inheritCheckupResult: Boolean? = null

    @Column(name = "payerAutoFilling")
    open var payerAutoFilling: Boolean? = null

    @Column(name = "dispByMobileTeam")
    open var dispByMobileTeam: Boolean? = null

    @Column(name = "filterPosts")
    open var filterPosts: Boolean? = null

    @Column(name = "filterSpecialities")
    open var filterSpecialities: Boolean? = null

    @Column(name = "compulsoryServiceStopIgnore")
    open var compulsoryServiceStopIgnore: Boolean? = null

    @Column(name = "voluntaryServiceStopIgnore")
    open var voluntaryServiceStopIgnore: Boolean? = null

    @Column(name = "inheritGoal", nullable = false)
    open var inheritGoal: Boolean? = false

    @Column(name = "MSE")
    open var mse: Boolean? = null

    @Column(name = "reqDN")
    open var reqDN: Boolean? = null

    @Column(name = "reqHealthGroup")
    open var reqHealthGroup: Boolean? = null

    @Column(name = "isAddTreatmentToDeath", nullable = false)
    open var isAddTreatmentToDeath: Boolean? = false

    @Column(name = "needReferal")
    open var needReferal: Boolean? = null

    @Column(name = "referalDateActualityDays")
    open var referalDateActualityDays: Int? = null

    @Column(name = "postfix", length = 200)
    open var postfix: String? = null

    @Column(name = "limitAnalysesActionsInput", nullable = false)
    open var limitAnalysesActionsInput: Boolean? = false

    @Column(name = "isAutoPrint")
    open var isAutoPrint: Boolean? = null

    @Column(name = "chk_ZNO")
    open var chkZno: Boolean? = null

    @Column(name = "chkMKB_ZNO")
    open var chkmkbZno: Boolean? = null

    @Column(name = "chkReason_ZNO")
    open var chkreasonZno: Boolean? = null

    @Column(name = "chkstady_ZNO")
    open var chkstadyZno: Boolean? = null

    @Column(name = "chkstady_T_ZNO")
    open var chkstadyTZno: Boolean? = null

    @Column(name = "chkstady_N_ZNO")
    open var chkstadyNZno: Boolean? = null

    @Column(name = "chkstady_M_ZNO")
    open var chkstadyMZno: Boolean? = null

    @Column(name = "chkDate_ZNO")
    open var chkdateZno: Boolean? = null

    @Column(name = "chkConsiliumData")
    open var chkConsiliumData: Boolean? = null

    @Column(name = "chkTransf", nullable = false)
    open var chkTransf: Boolean? = false

    @Column(name = "transfId")
    open var transfId: Int? = null

    @Column(name = "isWithoutResponsiblePerson")
    open var isWithoutResponsiblePerson: Boolean? = null

    @Column(name = "medicalPlace_id")
    open var medicalplaceId: Int? = null

    @Column(name = "chkF90", nullable = false)
    open var chkF90: Boolean? = false

    @Column(name = "canSend", nullable = false)
    open var canSend: Boolean? = false

    @Column(name = "hasDefaultSecretary", nullable = false)
    open var hasDefaultSecretary: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "defaultSecretary_id")
    open var defaultSecretary: PersonEntity? = null

    @Column(name = "allowCopyDiagnosis")
    open var allowCopyDiagnosis: Int? = null

    @Column(name = "begDateNotEditable", nullable = false)
    open var begDateNotEditable: Boolean? = false

    @Column(name = "endDateNotEditable", nullable = false)
    open var endDateNotEditable: Boolean? = false

    @Column(name = "isKslpShow")
    open var isKslpShow: Boolean? = null

    @Column(name = "isKSGCriterion")
    open var isKSGCriterion: Boolean? = null

    @Column(name = "isFilterVisitServiceByContract")
    open var isFilterVisitServiceByContract: Boolean? = null

    @Column(name = "netrica_Code", length = 65)
    open var netricaCode: String? = null

    @Column(name = "availableForExternal")
    open var availableForExternal: Boolean? = null

    @Column(name = "eventGoal")
    open var eventGoal: Int? = null

    @Column(name = "result")
    open var result: Int? = null

    @Column(name = "MKB", length = 8)
    open var mkb: String? = null

    @Column(name = "chk_SendInIEMK")
    open var chkSendiniemk: Boolean? = null

    @Column(name = "chkSurgeryCure")
    open var chkSurgeryCure: Boolean? = null

    @Column(name = "chkPillsTherapy")
    open var chkPillsTherapy: Boolean? = null

    @Column(name = "chkRadiationTherapy")
    open var chkRadiationTherapy: Boolean? = null

    @Column(name = "chkChemyTherapy")
    open var chkChemyTherapy: Boolean? = null

    @Column(name = "isSeveralEvents")
    open var isSeveralEvents: Boolean? = null
}