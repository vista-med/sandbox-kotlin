package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import javax.persistence.*

@Table(name = "ActionType")
@Entity
open class ActionTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @Column(name = "class", nullable = false)
    open var _class: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "group_id")
    open var group: ActionTypeEntity? = null

    @Column(name = "code", nullable = false)
    open var code: String? = null

    @Column(name = "name", nullable = false, length = 512)
    open var name: String? = null

    @Column(name = "title", nullable = false, length = 512)
    open var title: String? = null

    @Column(name = "needReferral")
    open var needReferral: Int? = null

    @Column(name = "flatCode", nullable = false, length = 512)
    open var flatCode: String? = null

    @Column(name = "sex", nullable = false)
    open var sex: Boolean? = false

    @Column(name = "age", nullable = false, length = 9)
    open var age: String? = null

    @Column(name = "office", nullable = false, length = 32)
    open var office: String? = null

    @Column(name = "showInForm", nullable = false)
    open var showInForm: Boolean? = false

    @Column(name = "genTimetable", nullable = false)
    open var genTimetable: Boolean? = false

    @Column(name = "context", nullable = false, length = 64)
    open var context: String? = null

    @Column(name = "amount", nullable = false)
    open var amount: Double? = null

    @Column(name = "amountEvaluation", nullable = false)
    open var amountEvaluation: Int? = null

    @Column(name = "defaultStatus", nullable = false)
    open var defaultStatus: Boolean? = false

    @Column(name = "defaultDirectionDate", nullable = false)
    open var defaultDirectionDate: Boolean? = false

    @Column(name = "defaultPlannedEndDate", nullable = false)
    open var defaultPlannedEndDate: Boolean? = false

    @Column(name = "defaultEndDate", nullable = false)
    open var defaultEndDate: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "defaultExecPerson_id")
    open var defaultExecPerson: PersonEntity? = null

    @ManyToOne
    @JoinColumn(name = "defaultSetPerson_id")
    open var defaultSetPerson: PersonEntity? = null

    @Column(name = "defaultPersonInEvent", nullable = false)
    open var defaultPersonInEvent: Boolean? = false

    @Column(name = "defaultPersonInEditor", nullable = false)
    open var defaultPersonInEditor: Boolean? = false

    @Column(name = "defaultMKB", nullable = false)
    open var defaultMKB: Boolean? = false

    @Column(name = "defaultMorphology", nullable = false)
    open var defaultMorphology: Boolean? = false

    @Column(name = "isMorphologyRequired", nullable = false)
    open var isMorphologyRequired: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "defaultOrg_id")
    open var defaultOrg: OrganisationEntity? = null

    @Column(name = "maxOccursInEvent", nullable = false)
    open var maxOccursInEvent: Int? = null

    @Column(name = "isMES")
    open var isMES: Int? = null

    @Column(name = "showTime", nullable = false)
    open var showTime: Boolean? = false

    @Column(name = "isPreferable", nullable = false)
    open var isPreferable: Boolean? = false

    @ManyToOne
    @JoinColumn(name = "prescribedType_id")
    open var prescribedType: ActionTypeEntity? = null

    @Column(name = "isRequiredCoordination", nullable = false)
    open var isRequiredCoordination: Boolean? = false

    @Column(name = "isNomenclatureExpense", nullable = false)
    open var isNomenclatureExpense: Boolean? = false

    @Column(name = "hasAssistant", nullable = false)
    open var hasAssistant: Boolean? = false

    @Column(name = "propertyAssignedVisible", nullable = false)
    open var propertyAssignedVisible: Boolean? = false

    @Column(name = "propertyUnitVisible", nullable = false)
    open var propertyUnitVisible: Boolean? = false

    @Column(name = "propertyNormVisible", nullable = false)
    open var propertyNormVisible: Boolean? = false

    @Column(name = "propertyEvaluationVisible", nullable = false)
    open var propertyEvaluationVisible: Boolean? = false

    @Column(name = "serviceType", nullable = false)
    open var serviceType: Boolean? = false

    @Column(name = "actualAppointmentDuration", nullable = false)
    open var actualAppointmentDuration: Int? = null

    @Column(name = "visible", nullable = false)
    open var visible: Boolean? = false

    @Column(name = "isSubstituteEndDateToEvent", nullable = false)
    open var isSubstituteEndDateToEvent: Boolean? = false

    @Column(name = "isPrinted", nullable = false)
    open var isPrinted: Boolean? = false

    @Column(name = "withoutAgree", nullable = false)
    open var withoutAgree: Boolean? = false

    @Column(name = "defaultMES", nullable = false)
    open var defaultMES: Boolean? = false

    @Column(name = "frequencyCount", nullable = false)
    open var frequencyCount: Int? = null

    @Column(name = "frequencyPeriod", nullable = false)
    open var frequencyPeriod: Boolean? = false

    @Column(name = "frequencyPeriodType", nullable = false)
    open var frequencyPeriodType: Boolean? = false

    @Column(name = "isStrictFrequency", nullable = false)
    open var isStrictFrequency: Boolean? = false

    @Column(name = "isFrequencyPeriodByCalendar", nullable = false)
    open var isFrequencyPeriodByCalendar: Boolean? = false

    @Column(name = "counter_id")
    open var counterId: Int? = null

    @Column(name = "period")
    open var period: Boolean? = null

    @Column(name = "singleInPeriod")
    open var singleInPeriod: Boolean? = null

    @Column(name = "checkPeriod", nullable = false)
    open var checkPeriod: Boolean? = false

    @Column(name = "isCustomSum", nullable = false)
    open var isCustomSum: Boolean? = false

    @Column(name = "recommendationExpirePeriod")
    open var recommendationExpirePeriod: Int? = null

    @Column(name = "recommendationControl")
    open var recommendationControl: Boolean? = null

    @Column(name = "lis_code", length = 32)
    open var lisCode: String? = null

    @Column(name = "isExecRequiredForEventExec", nullable = false)
    open var isExecRequiredForEventExec: Boolean? = false

    @Column(name = "isActiveGroup", nullable = false)
    open var isActiveGroup: Boolean? = false

    @Column(name = "locked", nullable = false)
    open var locked: Boolean? = false

    @Column(name = "filledLock")
    open var filledLock: Boolean? = null

    @ManyToOne
    @JoinColumn(name = "refferalType_id")
    open var refferalType: PersonEntity? = null

    @Column(name = "defaultBeginDate", nullable = false)
    open var defaultBeginDate: Boolean? = false

    @Column(name = "showAPOrg")
    open var showAPOrg: Boolean? = null

    @Column(name = "filterPosts")
    open var filterPosts: Boolean? = null

    @Column(name = "filterSpecialities")
    open var filterSpecialities: Boolean? = null

    @Column(name = "isIgnoreEventExecDate")
    open var isIgnoreEventExecDate: Boolean? = null

    @Column(name = "advancePaymentRequired")
    open var advancePaymentRequired: Boolean? = null

    @Column(name = "checkPersonSet")
    open var checkPersonSet: Boolean? = null

    @Column(name = "defaultIsUrgent", nullable = false)
    open var defaultIsUrgent: Boolean? = false

    @Column(name = "checkEnterNote")
    open var checkEnterNote: Boolean? = null

    @Column(name = "EGISZ_code", nullable = false, length = 512)
    open var egiszCode: String? = null

    @Column(name = "EGISZ_typecons_code")
    open var egiszTypeconsCode: String? = null

    @Column(name = "SMS")
    open var sms: String? = null

    @Column(name = "SEMD")
    open var semd: String? = null

    @Column(name = "consultationTypeId")
    open var consultationTypeId: Int? = null

    @Column(name = "formulaAlias", length = 64)
    open var formulaAlias: String? = null

    @Column(name = "instrumentalId")
    open var instrumentalId: Int? = null

    @Column(name = "old_group_id")
    open var oldGroupId: Int? = null

    @Column(name = "old_id")
    open var oldId: Int? = null
}