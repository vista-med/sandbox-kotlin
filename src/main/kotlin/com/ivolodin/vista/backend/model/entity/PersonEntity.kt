package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import java.time.LocalDate
import javax.persistence.*

@Table(name = "Person")
@Entity
open class PersonEntity(
    @Column(name = "lastName", nullable = false, length = 120)
    open val lastName: String,

    @Column(name = "firstName", nullable = false, length = 30)
    open val firstName: String,

    @Column(name = "patrName", nullable = false, length = 30)
    open val patrName: String,
    @Column(name = "createDatetime", nullable = false)
    open val createDatetime: Instant,

    @Column(name = "modifyDatetime", nullable = false)
    open val modifyDatetime: Instant,
) {

    fun getFullName() =
        "$lastName $firstName $patrName"

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @Column(name = "code", nullable = false, length = 12)
    open var code: String? = null

    @Column(name = "federalCode", nullable = false, length = 64)
    open var federalCode: String? = null

    @Column(name = "regionalCode", nullable = false, length = 16)
    open val regionalCode: String? = null


    @Column(name = "org_id")
    open var orgId: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orgStructure_id")
    open var orgStructure: OrgStructureEntity? = null

    @Column(name = "office", nullable = false, length = 8)
    open var office: String? = null

    @Column(name = "office2", nullable = false, length = 8)
    open var office2: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "finance_id")
    open var finance: RbFinanceEntity? = null

    @Column(name = "retireDate")
    open var retireDate: LocalDate? = null

    @Column(name = "ambPlan", nullable = false)
    open var ambPlan: Int? = null

    @Column(name = "ambPlan2", nullable = false)
    open var ambPlan2: Int? = null

    @Column(name = "ambNorm", nullable = false)
    open var ambNorm: Int? = null

    @Column(name = "homPlan", nullable = false)
    open var homPlan: Int? = null

    @Column(name = "homPlan2", nullable = false)
    open var homPlan2: Int? = null

    @Column(name = "homNorm", nullable = false)
    open var homNorm: Int? = null

    @Column(name = "expPlan", nullable = false)
    open var expPlan: Int? = null

    @Column(name = "expNorm", nullable = false)
    open var expNorm: Int? = null

    @Column(name = "login", nullable = false, length = 32)
    open var login: String? = null

    @Column(name = "password", nullable = false, length = 64)
    open var password: String? = null

    @Column(name = "retired", nullable = false)
    open var retired: Boolean? = false

    @Column(name = "birthDate", nullable = false)
    open var birthDate: LocalDate? = null

    @Column(name = "birthPlace", nullable = false, length = 128)
    open var birthPlace: String? = null

    @Column(name = "sex", nullable = false)
    open var sex: Boolean? = false

    @Column(name = "SNILS", nullable = false, length = 11)
    open var snils: String? = null

    @Column(name = "INN", nullable = false, length = 15)
    open var inn: String? = null

    @Column(name = "availableForExternal", nullable = false)
    open var availableForExternal: Int? = null

    @Column(name = "accessToDateSchedule")
    open var accessToDateSchedule: LocalDate? = null

    @Column(name = "depthDaysSchedule", nullable = false)
    open var depthDaysSchedule: Int? = null

    @Column(name = "lastAccessibleTimelineDate")
    open var lastAccessibleTimelineDate: LocalDate? = null

    @Column(name = "timelineAccessibleDays", nullable = false)
    open var timelineAccessibleDays: Int? = null

    @Column(name = "canSeeDays", nullable = false)
    open var canSeeDays: Int? = null

    @Column(name = "academicDegree", nullable = false)
    open var academicDegree: Boolean? = false

    @Column(name = "typeTimeLinePerson", nullable = false)
    open var typeTimeLinePerson: Int? = null

    @Column(name = "addComment", nullable = false)
    open var addComment: Boolean? = false

    @Column(name = "commentText", length = 200)
    open var commentText: String? = null

    @Column(name = "maritalStatus", nullable = false)
    open var maritalStatus: Int? = null

    @Column(name = "contactNumber", nullable = false, length = 15)
    open var contactNumber: String? = null

    @Column(name = "regType", nullable = false)
    open var regType: Boolean? = false

    @Column(name = "regBegDate")
    open var regBegDate: LocalDate? = null

    @Column(name = "regEndDate")
    open var regEndDate: LocalDate? = null

    @Column(name = "isReservist", nullable = false)
    open var isReservist: Boolean? = false

    @Column(name = "employmentType", nullable = false)
    open var employmentType: Boolean? = false

    @Column(name = "occupationType", nullable = false)
    open var occupationType: Boolean? = false

    @Column(name = "isDefaultInHB", nullable = false)
    open var isDefaultInHB: Boolean? = false

    @Column(name = "isInvestigator")
    open var isInvestigator: Boolean? = null

    @Column(name = "syncGUID", length = 36)
    open var syncGUID: String? = null

    @Column(name = "qaLevel")
    open var qaLevel: Boolean? = null

    @Lob
    @Column(name = "signature_cert")
    open var signatureCert: String? = null

    @Lob
    @Column(name = "signature_key")
    open var signatureKey: String? = null

    @Column(name = "doctorRoomAccessDenied")
    open var doctorRoomAccessDenied: Boolean? = null

    @Column(name = "ecp_password", length = 100)
    open var ecpPassword: String? = null

    @Column(name = "disableSignDoc")
    open var disableSignDoc: Boolean? = null

    @Column(name = "autoBalancingComplexity")
    open var autoBalancingComplexity: Boolean? = null

    @Column(name = "printer_id")
    open var printerId: Boolean? = null

    @Column(name = "IEMK_guid", length = 64)
    open var iemkGuid: String? = null

    @Column(name = "consultDepartment_id")
    open var consultdepartmentId: Int? = null

    @Column(name = "mse_speciality_id")
    open var mseSpecialityId: Int? = null

    @Column(name = "komtek_doctor_id")
    open var komtekDoctorId: Int? = null

    @Column(name = "komtek_user_id")
    open var komtekUserId: Int? = null

    @Column(name = "cashier_code")
    open var cashierCode: Int? = null

    @Column(name = "grkmGUID", length = 45)
    open var grkmGUID: String? = null

    @Column(name = "qualification", length = 100)
    open var qualification: String? = null

    @Column(name = "ready_to_online_consultation")
    open var readyToOnlineConsultation: Int? = null

    @Column(name = "email", length = 100)
    open var email: String? = null

    @Column(name = "tadam_username", length = 100)
    open var tadamUsername: String? = null

    @Column(name = "tadam_password", length = 100)
    open var tadamPassword: String? = null

    @Column(name = "dontSendVIMIS", nullable = false)
    open var dontSendVIMIS: Boolean? = false
}