package com.ivolodin.vista.backend.model

class ApiException(message: String?) : RuntimeException(message) {
}