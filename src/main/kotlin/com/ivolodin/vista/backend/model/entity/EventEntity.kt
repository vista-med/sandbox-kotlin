package com.ivolodin.vista.backend.model.entity

import java.time.LocalDateTime
import javax.persistence.*

@Table(name = "Event")
@Entity
open class EventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: LocalDateTime? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: LocalDateTime? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "deleted", nullable = false)
    open var deleted: Boolean? = false

    @Column(name = "externalId", nullable = false, length = 30)
    open var externalId: String? = null

    @ManyToOne(optional = false)
    @JoinColumn(name = "eventType_id", nullable = false)
    open var eventType: EventTypeEntity? = null

    @Column(name = "org_id")
    open var orgId: Int? = null

    @ManyToOne
    @JoinColumn(name = "client_id")
    open var client: ClientEntity? = null

    @Column(name = "prevEventDate")
    open var prevEventDate: LocalDateTime? = null

    @Column(name = "setDate", nullable = false)
    open var setDate: LocalDateTime? = null

    @ManyToOne
    @JoinColumn(name = "setPerson_id")
    open var setPerson: PersonEntity? = null

    @Column(name = "execDate")
    open var execDate: LocalDateTime? = null

    @ManyToOne
    @JoinColumn(name = "execPerson_id")
    open var execPerson: PersonEntity? = null

    @Column(name = "isPrimary", nullable = false)
    open var isPrimary: Boolean? = false

    @Column(name = "`order`", nullable = false)
    open var order: Boolean? = false

    @Column(name = "nextEventDate")
    open var nextEventDate: LocalDateTime? = null

    @Column(name = "payStatus", nullable = false)
    open var payStatus: Int? = null

    @Lob
    @Column(name = "note", nullable = false)
    open var note: String? = null

    @ManyToOne
    @JoinColumn(name = "curator_id")
    open var curator: PersonEntity? = null

    @ManyToOne
    @JoinColumn(name = "assistant_id")
    open var assistant: PersonEntity? = null

    @Column(name = "pregnancyWeek", nullable = false)
    open var pregnancyWeek: Int? = null

    @Column(name = "MES_id")
    open var mesId: Int? = null

    @Column(name = "HTG_id")
    open var htgId: Int? = null

    @Column(name = "KSG_id")
    open var ksgId: Int? = null

    @ManyToOne
    @JoinColumn(name = "relegateOrg_id")
    open var relegateOrg: OrganisationEntity? = null

    @Column(name = "totalCost", nullable = false)
    open var totalCost: Double? = null

    @ManyToOne
    @JoinColumn(name = "prevEvent_id")
    open var prevEvent: EventEntity? = null

    @Column(name = "armyReferral_id")
    open var armyreferralId: Int? = null

    @Column(name = "outgoingRefNumber", length = 10)
    open var outgoingRefNumber: String? = null

    @Column(name = "eventCostPrinted", nullable = false)
    open var eventCostPrinted: Boolean? = false

    @Column(name = "exposeConfirmed", nullable = false)
    open var exposeConfirmed: Boolean? = false

    @Column(name = "ZNOFirst")
    open var zNOFirst: Boolean? = null

    @Column(name = "ZNOMorph")
    open var zNOMorph: Boolean? = null

    @Column(name = "hospParent", nullable = false)
    open var hospParent: Boolean? = false

    @Column(name = "cycleDay")
    open var cycleDay: Int? = null

    @Column(name = "locked")
    open var locked: Boolean? = null

    @Column(name = "dispByMobileTeam")
    open var dispByMobileTeam: Boolean? = null

    @Column(name = "duration")
    open var duration: Int? = null

    @Column(name = "isClosed", nullable = false)
    open var isClosed: Boolean? = false

    @Column(name = "chemoName", length = 50)
    open var chemoName: String? = null

    @Column(name = "chemoCourse", length = 50)
    open var chemoCourse: String? = null

    @Column(name = "doesPatientNeedToSickLeave")
    open var doesPatientNeedToSickLeave: Boolean? = null

    @Column(name = "orgStructure_id")
    open var orgstructureId: Int? = null

    @Column(name = "MSE")
    open var mse: Boolean? = null

    @Column(name = "vista_system")
    open var vistaSystem: Int? = null

    @Column(name = "isStage")
    open var isStage: Boolean? = null

    @Column(name = "isCrime")
    open var isCrime: Boolean? = null

    @Column(name = "signedDocuments", nullable = false)
    open var signedDocuments: Boolean? = false

    @Column(name = "signDateTime")
    open var signDateTime: LocalDateTime? = null

    @Column(name = "chemoComment", length = 100)
    open var chemoComment: String? = null

    @Column(name = "doseReduction", length = 100)
    open var doseReduction: String? = null

    @Column(name = "doseReductionReason", length = 100)
    open var doseReductionReason: String? = null

    @Column(name = "drugScheme_id")
    open var drugschemeId: Int? = null

    @Column(name = "journalNum")
    open var journalNum: Int? = null

    @Column(name = "fixate", nullable = false)
    open var fixate: Boolean? = false

    @Column(name = "parentEvent_id")
    open var parenteventId: Int? = null

    @Column(name = "transfId")
    open var transfId: Int? = null

    @ManyToOne
    @JoinColumn(name = "secretary_id")
    open var secretary: PersonEntity? = null

    @Column(name = "UUID", length = 36)
    open var uuid: String? = null

    @Column(name = "KSGCriterion")
    open var kSGCriterion: Int? = null

    @Column(name = "kslp_coefficient")
    open var kslpCoefficient: Double? = null

    @Column(name = "isMovedToMSE")
    open var isMovedToMSE: Int? = null

    @Column(name = "FLCStatus")
    open var fLCStatus: Int? = null

    @Column(name = "komtek_id")
    open var komtekId: Int? = null
}