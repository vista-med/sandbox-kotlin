package com.ivolodin.vista.backend.model.entity

import java.time.Instant
import javax.persistence.*

@Table(name = "rbFinance")
@Entity
open class RbFinanceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int? = null

    @Column(name = "createDatetime", nullable = false)
    open var createDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "createPerson_id")
    open var createPerson: PersonEntity? = null

    @Column(name = "modifyDatetime", nullable = false)
    open var modifyDatetime: Instant? = null

    @ManyToOne
    @JoinColumn(name = "modifyPerson_id")
    open var modifyPerson: PersonEntity? = null

    @Column(name = "code", nullable = false, length = 8)
    open var code: String? = null

    @Column(name = "idx")
    open var idx: Int? = null

    @Column(name = "name", nullable = false, length = 64)
    open var name: String? = null

    @Column(name = "netrica_Code")
    open var netricaCode: String? = null

    @Column(name = "EGISZ_code", length = 16)
    open var egiszCode: String? = null

    @Lob
    @Column(name = "EGISZ_name")
    open var egiszName: String? = null
}