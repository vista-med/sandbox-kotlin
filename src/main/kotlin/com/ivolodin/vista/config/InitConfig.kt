package com.ivolodin.vista.config

import eu.vaadinonkotlin.VaadinOnKotlin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@EnableSwagger2
@Configuration
class InitConfig {
    init {
        //used for initializing VaadinOnKotlin plugins.
        // Have to do it manually, rather than automatic, due to last is not working
        VaadinOnKotlin.init()
    }

    @Bean
    fun swaggerDoc(): Docket = Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.withClassAnnotation(RestController::class.java))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo())

    private fun apiInfo() =
        ApiInfoBuilder().title("Vista med application")
            .description("Searches for information about doctors and clients ")
            .build()
}
