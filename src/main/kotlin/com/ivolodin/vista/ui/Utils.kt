package com.ivolodin.vista.ui

import com.github.mvysny.karibudsl.v10.VaadinDsl
import com.github.mvysny.karibudsl.v10.init
import com.ivolodin.vista.ui.components.SearchToolbar
import com.vaadin.componentfactory.Autocomplete
import com.vaadin.flow.component.HasComponents

@VaadinDsl
fun (@VaadinDsl HasComponents).searchToolbarView(block: (@VaadinDsl SearchToolbar).() -> Unit = {}) =
    init(SearchToolbar(), block)

@VaadinDsl
fun (@VaadinDsl HasComponents).autocompleteField(
    limit: Int = 5,
    block: (@VaadinDsl Autocomplete).() -> Unit = {}
): Autocomplete = init(Autocomplete(limit), block)