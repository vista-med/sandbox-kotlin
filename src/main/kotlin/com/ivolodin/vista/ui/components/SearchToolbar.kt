package com.ivolodin.vista.ui.components

import com.github.mvysny.karibudsl.v10.KComposite
import com.github.mvysny.karibudsl.v10.button
import com.github.mvysny.karibudsl.v10.datePicker
import com.github.mvysny.karibudsl.v10.div
import com.ivolodin.vista.ui.autocompleteField
import com.vaadin.componentfactory.Autocomplete
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import java.time.LocalDate

class SearchToolbar : KComposite() {
    var nameFieldValue: String = ""
    lateinit var dateToShow: DatePicker
    private lateinit var nameButton: Autocomplete

    var onSearch: () -> Unit = {}
    var dataSource: () -> List<String> = { emptyList() }

    private val root = ui {
        div("search-toolbar") {
            nameButton = autocompleteField {
                width = "300px"
                setPlaceholder("First Last Patronymic")
                addChangeListener { event ->
                    nameFieldValue = event.value
                    this.options = dataSource.invoke()
                }
                addValueChangeListener { nameFieldValue = it.value }
                addValueClearListener { nameFieldValue = "" }
                isVisible = true
            }
            dateToShow = datePicker {
                value = LocalDate.of(2021, 2, 9)
                placeholder = "Date to show"
                addClassName("view-toolbar-field")
            }
            button("Search", Icon(VaadinIcon.SEARCH)) {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY)
                addClickListener { onSearch() }
            }
        }
    }
}