package com.ivolodin.vista.ui.view

import com.github.mvysny.karibudsl.v10.*
import com.ivolodin.vista.backend.model.request.SearchRequest
import com.ivolodin.vista.backend.model.response.SearchResponse
import com.ivolodin.vista.ui.client.SearchClient
import com.ivolodin.vista.ui.components.SearchToolbar
import com.ivolodin.vista.ui.searchToolbarView
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.START
import com.vaadin.flow.router.Route
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

@CssImport("/styles/styles.css")
@Route("")
class MainView : KComposite() {
    private val searchClient = SearchClient()
    private lateinit var searchToolbar: SearchToolbar
    private lateinit var grid: Grid<SearchResponse>
    private val root = ui {
        verticalLayout {
            h1("Hello, mazafaka. You are here to suck dicks from Kotlin, Spring and Vaadin. Welcome to hell") {
                alignSelf = START
            }
            alignItems = CENTER
            horizontalLayout {
                alignItems = CENTER
                searchToolbar = searchToolbarView {
                    onSearch = { searchForPatients() }
                    dataSource = { searchForDoctors() }
                }
            }
            grid = grid {
                width = "50%"
                alignSelf = CENTER
                addColumn { it.getDoctorFullName() }.apply {
                    isAutoWidth = true
                    setHeader("Doctor")
                    isSortable = true
                }
                addColumn { it.getClientFullName() }.apply {
                    setHeader("Patient")
                    isAutoWidth = true
                    isSortable = true
                }
                addColumn { it.eventDate.toRussianDate() }.apply {
                    isAutoWidth = true
                    setHeader("Date")
                }
            }

        }
    }

    private fun searchForDoctors(): List<String> = searchClient.getDoctorList(searchToolbar.nameFieldValue)

    private fun searchForPatients() {
        val response = searchClient.getPatientList(buildSearchPatientRequest())
        grid.setItems(response)
    }

    private fun buildSearchPatientRequest(): SearchRequest {
        return SearchRequest(
            doctorFullName = searchToolbar.nameFieldValue,
            dateToShow = searchToolbar.dateToShow.value
        )
    }

}

private fun LocalDateTime?.toRussianDate(): String {
    return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
        .withZone(ZoneId.systemDefault())
        .withLocale(Locale("ru"))
        .format(this) ?: ""
}
