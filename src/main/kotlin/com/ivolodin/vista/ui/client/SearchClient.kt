package com.ivolodin.vista.ui.client

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ivolodin.vista.backend.model.request.SearchRequest
import com.ivolodin.vista.backend.model.response.SearchResponse
import com.ivolodin.vista.ui.client.converter.GsonLocalDateConverter
import com.ivolodin.vista.ui.client.converter.GsonLocalDateTimeConverter
import eu.vaadinonkotlin.restclient.OkHttpClientVokPlugin
import eu.vaadinonkotlin.restclient.exec
import eu.vaadinonkotlin.restclient.jsonArray
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.time.LocalDate
import java.time.LocalDateTime

class SearchClient(val baseUrl: String = "http://localhost:8080") {
    private val client: OkHttpClient = OkHttpClientVokPlugin.okHttpClient!!
    private val jsonBuilder: Gson = GsonBuilder()
        .registerTypeAdapter(LocalDate::class.java, GsonLocalDateConverter())
        .registerTypeAdapter(LocalDateTime::class.java, GsonLocalDateTimeConverter())
        .create()

    fun getPatientList(searchRequest: SearchRequest): List<SearchResponse> {
        val jsonBody = jsonBuilder
            .toJson(searchRequest)
        val request = Request.Builder()
            .url("$baseUrl/search")
            .post(jsonBody.toRequestBody("application/json".toMediaType()))
            .build()

        return client.exec(request) { responseBody -> responseBody.jsonArray(SearchResponse::class.java) }
    }

    fun getDoctorList(nameFieldValue: String): List<String> {
        val url = "$baseUrl/doctor?name=$nameFieldValue".toHttpUrl()
        val request = Request.Builder()
            .url(url)
            .get()
            .build()
        return client.exec(request) { responseBody -> responseBody.jsonArray(String::class.java) }
    }
}
