import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.5.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.vaadin") version "0.14.6.0"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
    kotlin("plugin.jpa") version "1.5.31"
}

group = "com.ivolodin"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven("https://vaadin.com/nexus/content/repositories/vaadin-addons/")
}

extra["vaadinVersion"] = "14.7.1"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.6.1")
    implementation("org.springframework.boot:spring-boot-starter-web:2.6.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.0")
    implementation("com.vaadin:vaadin-spring-boot-starter:22.0.1")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.0")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.0")
    implementation("org.projectlombok:lombok:1.18.22")
    implementation("com.squareup.okhttp3:okhttp:4.9.3")
    implementation("com.google.code.gson:gson:2.8.9")
    implementation("com.fatboyindustrial.gson-javatime-serialisers:gson-javatime-serialisers:1.1.1")
    implementation("eu.vaadinonkotlin:vok-rest-client:0.11.2")
    implementation("io.springfox:springfox-boot-starter:3.0.0")
    implementation("com.vaadin.componentfactory:autocomplete:2.3.2")

    developmentOnly("org.springframework.boot:spring-boot-devtools:2.6.1")

    compileOnly("com.github.mvysny.karibudsl:karibu-dsl:1.1.1")

    runtimeOnly("org.mariadb.jdbc:mariadb-java-client:2.7.3")

    testImplementation("org.springframework.boot:spring-boot-starter-test:2.6.1")
}

dependencyManagement {
    imports {
        mavenBom("com.vaadin:vaadin-bom:${property("vaadinVersion")}")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
